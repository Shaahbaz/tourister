<?php

namespace Magebay\Bookingsystem\Block\Adminhtml;
 
use Magento\Backend\Block\Template;
use Magebay\Bookingsystem\Helper\BkHelperDate;
use Magebay\Bookingsystem\Model\CalendarsFactory;
use Magento\Framework\Pricing\Helper\Data as PriceHelper;
use Magento\Framework\Stdlib\DateTime\Timezone;
use Magento\Directory\Model\Currency;

class Calendars extends Template
{
	/**
     * @param \Magebay\Bookingsystem\Helper\BkHelperDate
     * 
     */
	protected $_bkHelperDate;
	/**
     * Result page factory
     *
     * @var \Magebay\Bookingsystem\Model\CalendarsFactory;
     */
	protected $_calendarsFactory;
	/**
     * @var PriceHelper
     */
	protected $_priceHelper;
	/**
     * @var Magento\Framework\Stdlib\DateTime\Timezone
     */
	protected $_timeZone;
	 /**
     * Result page factory
     *
     * @var \Magento\Directory\Model\Currency;
     */
	protected $_currency;
	function __construct(
		\Magento\Backend\Block\Widget\Context $context,
		BkHelperDate $bkHelperDate,
		CalendarsFactory $calendarsFactory,
		PriceHelper $priceHelper,
		Timezone $timezone,
		Currency $currency,
		array $data = []
	)
	{
		parent::__construct($context, $data);
		$this->_bkHelperDate = $bkHelperDate;
		$this->_calendarsFactory = $calendarsFactory;
		$this->_priceHelper = $priceHelper;
		$this->_timeZone = $timezone;
		$this->_currency = $currency;
		
	}
	function getBkAjaxUrl()
	{
		$bkHelperDate = $this->getBkHelperDate();
		$urlcalendar = $bkHelperDate->getBkAdminAjaxUrl('bookingsystem/booking/calendars');
		$urlEdit = $bkHelperDate->getBkAdminAjaxUrl('bookingsystem/booking/calendarEdit');
		$urlDelete = $bkHelperDate->getBkAdminAjaxUrl('bookingsystem/booking/calendarDelete');
		$urlLoadItem = $bkHelperDate->getBkAdminAjaxUrl('bookingsystem/booking/calendarItems');
		$urlSaveItem = $bkHelperDate->getBkAdminAjaxUrl('bookingsystem/booking/calendarSave');
		return array(
			'url_calendars'=>$urlcalendar,
			'edit'=>$urlEdit,
			'dell'=>$urlDelete,
			'load_items'=>$urlLoadItem,
			'url_calendars'=>$urlcalendar,
			'save_calendar'=>$urlSaveItem,
		);
	}
	/**
	* get items for calendars
	* @param int $bookingId, string $bookingType = per_day
	* @return array $items
	**/
	function getBkItems()
	{
		$arData = array();
		$bookingId = $this->getBookingId();
		$bookingType = $this->getBookingType();
		$calendars = array();
		$model = $this->_calendarsFactory->create();
		$calendars = $model->getBkCalendarsById($bookingId,array('*'),array('calendar_booking_type'=>$bookingType));
		return $calendars;
	}
	/**
	* get item for from edit 
	* @param int $calendarId
	* @return array $item
	**/
	function getBkItem()
	{
		$calendarId = $this->_request->getParam('calendar_id',0);
		//if get date from calendar
		$checkIn = $this->_request->getParam('check_in','');
		$checkOut = $this->_request->getParam('check_out','');
		$bookingType =  $this->_request->getParam('booking_type','per_day');
		$calendar = null;
		if($calendarId > 0)
		{
			$model = $this->_calendarsFactory->create();
			$calendar = $model->load($calendarId);
		}
		elseif($checkIn != '' && $checkOut != '')
		{
			$model = $this->_calendarsFactory->create();
			$collection = $model->getCollection()
					->addFieldToFilter('calendar_booking_type',$bookingType)
					->addFieldToFilter('calendar_startdate',array('lteq'=>$checkIn))
					->addFieldToFilter('calendar_enddate',array('gteq'=>$checkOut));
			if(count($collection))
			{
				$calendar = $collection->getFirstItem();
			}
		}
		return $calendar;
	}
	/**
	* get items for calendars to validate data
	* @param int $bookingId, string $bookingType
	* @return array $items
	**/
	function getBkCalendars()
	{
		$bookingId = $this->getBookingId();
		$bookingType = $this->getBookingType();
		$model = $this->_calendarsFactory->create();
		$calendars = $model->getBkCalendarsById($bookingId,array('*'),array('calendar_booking_type'=>$bookingType));
		return $calendars;
	}
	function getBkHelperDate()
	{
		return $this->_bkHelperDate;
	}
	/**
	* get price helper
	**/
	function getBkPriceHelper()
	{
		return $this->_priceHelper;
	}
	function getBkCurrentDate()
	{
		$intCurrentTime = $this->_timeZone->scopeTimeStamp();
		$currDate = date('Y-m-d',$intCurrentTime);
		return $currDate;
	}
	function getBkCurrencySymbol()
	{
		return $this->_currency->getCurrencySymbol();
	}
	function getBkRequest()
	{
		return $this->_request;
	}
}