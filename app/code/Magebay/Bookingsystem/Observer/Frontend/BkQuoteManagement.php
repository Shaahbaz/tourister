<?php

namespace Magebay\Bookingsystem\Observer\Frontend;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Checkout\Model\Cart as BkCoreCart;
use Magento\Quote\Model\Quote\Item\OptionFactory as QuoteItemOptionFactory;
use Magebay\Bookingsystem\Model\BookingsFactory;
use Magebay\Bookingsystem\Model\BookingordersFactory;
use Magebay\Bookingsystem\Helper\BkHelperDate;
use Magebay\Bookingsystem\Helper\BkSimplePriceHelper;
use Magebay\Bookingsystem\Helper\BkCustomOptions;

class BkQuoteManagement implements ObserverInterface
{
	/**
	* @var BkCoreCart;
	**/
	protected $_bkCoreCart;
	/**
	* @var QuoteItemOptionFactory;
	**/
	protected $_quoteItemOptionFactory;
	/**
	* @var Magebay\Bookingsystem\Model\BookingsFactory;
	**/
	protected $_bookingFactory;
	/**
	* @var Magebay\Bookingsystem\Model\Bookingorders;
	**/
	protected $_bookingorders;
	/**
	* @var Magebay\Bookingsystem\Helper\BkHelperDate;
	**/
	protected $_bkHelperDate;
	/**
	* @var Magebay\Bookingsystem\Helper\BkSimplePriceHelper;
	**/
	protected $_bkSimplePriceHelper;
	/**
	* @var Magebay\Bookingsystem\Helper\BkCustomOptions;
	**/
	protected $_bkCustomOptions;
	public function __construct(
		BkCoreCart $bkCoreCart,
		QuoteItemOptionFactory $quoteItemOptionFactory,
		BookingsFactory $bookingFactory,
		BookingordersFactory $bookingorders,
		BkHelperDate $bkHelperDate,
		BkSimplePriceHelper $bkSimplePriceHelper,
		BkCustomOptions $bkCustomOptions
	)
    {
		$this->_bkCoreCart = $bkCoreCart;
		$this->_quoteItemOptionFactory = $quoteItemOptionFactory;
		$this->_bookingFactory = $bookingFactory;
        $this->_bookingorders = $bookingorders;
		$this->_bkHelperDate = $bkHelperDate;
		$this->_bkSimplePriceHelper = $bkSimplePriceHelper;
		$this->_bkCustomOptions = $bkCustomOptions;
    }
	public function execute(EventObserver $observer)
    {
		$enable = $this->_bkHelperDate->getFieldSetting('bookingsystem/setting/enable');
		if($enable == 1)
		{
			//get order
			$order = $observer->getOrder();
			$customerName = '';
			if($order->getShippingAddress())
			{
				$shiipingAddress = $order->getShippingAddress();
				$customerName = $shiipingAddress->getFirstname().' '.$shiipingAddress->getMiddlename().' '.$shiipingAddress->getLastname();
			}
			elseif($order->getBillingAddress())
			{
				$bildingAddress = $order->getBillingAddress();
				$customerName = $bildingAddress->getFirstname().' '.$bildingAddress->getMiddlename().' '.$bildingAddress->getLastname();
			}
			$items = $order->getAllVisibleItems();
			if(count($items))
			{
				foreach($items as $item)
				{
					$_product = $item->getProduct();
					if($_product->getTypeId() == 'booking')
					{
						$dataSave = array();
						//booking simple
						$checkIn = '';
						$checkOut = '';
						$serviceStart = '';
						$serviceEnd = '';
						$totalDays = 0;
						$totalHours = 0;
						//special for intervals hours type
						$interQty = 0;
						$strInterTime = '';
						$requestOptions = $item->getProductOptionByCode('info_buyRequest');
						// $quantity = $requestOptions['qty'];
						$quantity = $item->getQtyOrdered();
						$paramAddons = isset($requestOptions['addons']) ? $requestOptions['addons'] : array();
						$arPrice = array();
						$arPrice['str_error'] = '';
						$bookingModel = $this->_bookingFactory->create();
						$booking = $bookingModel->getBooking($_product->getId());
						if($booking && $booking->getBookingType() == 'per_day')
						{
							$checkIn = $this->_bkHelperDate->convertFormatDate($requestOptions['check_in']);
							if(isset($requestOptions['check_out']))
							{
								$checkOut = $this->_bkHelperDate->convertFormatDate($requestOptions['check_out']);
							}
							else
							{
								$checkOut = $checkIn;
							}
							if($booking->getBookingTime() == 1)
							{
								$arPrice = $this->_bkSimplePriceHelper->getPriceBetweenDays($booking,$checkIn,$checkOut,$quantity,$item->getItemId(),$paramAddons);
							}
							elseif($booking->getBookingTime() == 2)
							{
								$serviceStart = $requestOptions['from_time_h'];
								$serviceStart .= ','.$requestOptions['from_time_m'];
								$serviceStart .= ','.$requestOptions['from_time_t'];
								$serviceEnd = $requestOptions['to_time_h'];
								$serviceEnd .= ','.$requestOptions['to_time_m'];
								$serviceEnd .= ','.$requestOptions['to_time_t'];
								$fromHour =  $requestOptions['from_time_t'] == 1 ? $requestOptions['from_time_h'] : ($requestOptions['from_time_h'] + 12);
								$toHour =  $requestOptions['to_time_t'] == 1 ? $requestOptions['to_time_h'] : ($requestOptions['to_time_h'] + 12);
								$arPrice = $this->_bkSimplePriceHelper->getHourPriceBetweenDays($booking,$checkIn,$checkOut,$fromHour,$toHour,$requestOptions['from_time_m'],$requestOptions['to_time_m'],$quantity,$item->getItemId(),$paramAddons);
							}
							elseif($booking->getBookingTime() == 3)
							{
								$interQty = $quantity;
								$intervalsHours = $requestOptions['intervals_hours'];
								if(count($intervalsHours))
								{
									$strInterTime = implode(',',$requestOptions['intervals_hours']);
								}
								
							}
							if(count($arPrice) && $arPrice['str_error'] == '')
							{
								$totalDays = isset($arPrice['total_days']) ? $arPrice['total_days'] : 0;
								$totalHours = isset($arPrice['total_hours']) ? $arPrice['total_hours'] : 0;
							}
							$dataSave = array(
								'bkorder_check_in'=>$checkIn,
								'bkorder_check_out'=>$checkOut,
								'bkorder_qty'=>$quantity,
								'bkorder_customer'=>$customerName,
								'bkorder_booking_id'=>$_product->getId(),
								'bkorder_order_id'=>$order->getId(),
								'bkorder_room_id'=>0,
								'bkorder_service_start'=>$serviceStart,
								'bkorder_service_end'=>$serviceEnd,
								'bkorder_total_days'=>$totalDays,
								'bkorder_total_hours'=>$totalHours,
								'bkorder_qt_item_id'=>$item->getItemId(),
								'bkorder_quantity_interval'=>$interQty,
								'bkorder_interval_time'=>$strInterTime,
							);
						}
						else
						{
							$checkIn = $this->_bkHelperDate->convertFormatDate($requestOptions['room_check_in']);
							$checkOut =  $this->_bkHelperDate->convertFormatDate($requestOptions['room_check_out']);
							$totalDays = (strtotime($checkOut) - strtotime($checkIn)) / (24 * 60 * 60);
							$dataSave = array(
								'bkorder_check_in'=>$checkIn,
								'bkorder_check_out'=>$checkOut,
								'bkorder_qty'=>$quantity,
								'bkorder_customer'=>$customerName,
								'bkorder_booking_id'=>$requestOptions['room_id'],
								'bkorder_order_id'=>$order->getId(),
								'bkorder_room_id'=>1,
								'bkorder_service_start'=>$serviceStart,
								'bkorder_service_end'=>$serviceEnd,
								'bkorder_total_days'=>$totalDays,
								'bkorder_total_hours'=>$totalHours,
								'bkorder_qt_item_id'=>$item->getItemId(),
								'bkorder_quantity_interval'=>$interQty,
								'bkorder_interval_time'=>$strInterTime,
							);
						}
						//convet quote item to order item
						/* $quoteItemOptionModel = $this->_quoteItemOptionFactory->create();
						$quoteItemOptionCollection = $quoteItemOptionModel->getCollection()
								->addFieldToFilter('item_id',$item->getQuoteItemId())
								->addFieldToFilter('code','additional_options');
						$quoteItemOption = $quoteItemOptionCollection->getFirstItem();
						if($quoteItemOption->getId())
						{
							$additionalOptions['info_buyRequest'] = $requestOptions;
							$additionalOptions['additional_options'] = unserialize($quoteItemOption->getValue());
							$item->setProductOptions($additionalOptions);
							$item->save();
						} */
						//additional_options
						if(isset($requestOptions['is_bk_back_end']) && $requestOptions['is_bk_back_end'] == 1)
						{
							$requestOptions['backend_item_id'] = $item->getQuoteItemId();
						}
						$aradditionalOptions = $this->_bkCustomOptions->createExtractOptions($_product,$requestOptions);
						//delete old order 
						$isDeleteOldOrder = false;
						$oldBkOrderItem = 0;
						if(isset($requestOptions['is_bk_back_end']) && (isset($requestOptions['bk_order_item_id']) && $requestOptions['bk_order_item_id'] != $item->getItemId()))
						{
							$oldBkOrderItem = $requestOptions['bk_order_item_id'];
							$isDeleteOldOrder = true;
						}
						$requestOptions['bk_order_item_id'] = $item->getItemId();
						$additionalOptions['info_buyRequest'] = $requestOptions;
						$additionalOptions['additional_options'] = $aradditionalOptions['bk_options'];
						$item->setProductOptions($additionalOptions);
						$item->save();
						if(count($dataSave))
						{
							$bkOrderModel = $this->_bookingorders->create();
							$bkOrderModel->setData($dataSave)->save();
							//delete old if edit item if backend
							if(isset($isDeleteOldOrder))
							{
								$tempCollection = $bkOrderModel->getCollection()
										->addFieldToFilter('bkorder_qt_item_id',$oldBkOrderItem);
								if(count($tempCollection))
								{
									$tempOldBOrder = $tempCollection->getFirstItem();
									if($tempOldBOrder && $tempOldBOrder->getId())
									{
										$bkOrderModel->setId($tempOldBOrder->getId())->delete();
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
}
