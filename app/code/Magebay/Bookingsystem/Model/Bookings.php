<?php
 
namespace Magebay\Bookingsystem\Model;
 
use Magento\Framework\Model\AbstractModel;

use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\App\ResourceConnection; 
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\Db;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Catalog\Model\Product as ProductModel;
class Bookings extends AbstractModel
{
	/**
     *
     * @var Magento\Framework\App\ResourceConnection
    */
	protected $_resourceConnection;
	protected $_productModel;
	protected $_countryCollection;
    /**

     * @param IsActive $statusList
     * @param Context $context
     * @param Registry $registry
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
		ProductModel $productModel,
		ResourceConnection $resourceConection,
		\Magento\Directory\Model\ResourceModel\Country\Collection $countryCollection,
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
		$this->_productModel = $productModel;
		$this->_resourceConnection = $resourceConection;
		$this->_countryCollection = $countryCollection;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
		
    }
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Magebay\Bookingsystem\Model\ResourceModel\Bookings');
    }
	/**
	* get all bookings
	* return array $bookings
	**/
	function getBookings($arrayAttributeSelect = array('*'),$arAttributeConditions = array(),$condition = '',$arrayBooking = array('*'),$attributeSort = array(),$bookingSort = '',$limit = 0,$curPage = 1)
	{
		$resourceConnection = $this->_resourceConnection;
		$tableAlias = $resourceConnection->getTableName('booking_systems');
		$modelProduct = $this->_productModel;
		$collection = $modelProduct->getCollection()
				->addAttributeToSelect($arrayAttributeSelect);
		if(count($arAttributeConditions))
		{
			foreach($arAttributeConditions as $keyAtt => $attCondition)
			{
				$collection->addAttributeToFilter($keyAtt,$attCondition);
			}
		}
		$collection->getSelect()->join($tableAlias,'e.entity_id = '.$tableAlias.'.booking_product_id',$arrayBooking);
		if(count($attributeSort))
		{
			$collection->addAttributeToSort($attributeSort['filed'],$attributeSort['sort']);	
		}
		$collection->getSelect()->where($tableAlias.'.booking_product_id IS NOT NULL');
		if($condition != '')
		{
			$collection->getSelect()->where($condition);
			
		}
		if($bookingSort != '')
		{
			$collection->getSelect()->order($tableAlias.'.'.$bookingSort);
		}
		if($limit > 0)
		{
			$collection->setPageSize($limit);
		}
		$collection->setCurPage($curPage);
		return $collection;
	}
	/* get booking item 
	* @return $booking
	*/
	function getBooking($productId,$arrayAttributeSelect = array('*'),$arAttributeConditions = array(),$condition = '',$arrayBooking = array('*'),$attributeSort = array(),$bookingSort = '',$limit = 0,$curPage = 1)
	{
		$arAttributeConditions = array('entity_id'=>$productId);
		$collection = $this->getBookings($arrayAttributeSelect,$arAttributeConditions,$condition,$arrayBooking,$attributeSort,$bookingSort,$limit,$curPage);
		$collection = $collection->getFirstItem();
		return $collection;
	}
	function saveBooking($bookingParams,$productId)
	{
		$serviceStart = '';
		$serviceEnd = '';
		if($bookingParams['booking_time'] == '2' || $bookingParams['booking_time'] == '3')
		{
			$arServiceStart = $bookingParams['booking_service_start'];
			$serviceStart = $arServiceStart['hour'].','.$arServiceStart['minute'].','.$arServiceStart['type'];
			$arServiceEnd = $bookingParams['booking_service_end'];
			$serviceEnd = $arServiceEnd['hour'].','.$arServiceEnd['minute'].','.$arServiceEnd['type'];
		}
		$bookingParams['booking_service_start'] = $serviceStart;
		$bookingParams['booking_service_end'] = $serviceEnd;
		$bookingParams['booking_id'] = $bookingParams['booking_temp_id'];
		$bookingParams['booking_product_id'] = $productId;
		if(isset($bookingParams['booking_state_id']) && (int)$bookingParams['booking_state_id'] > 0)
		{
			$bookingParams['booking_state'] = '';
		}
		if($bookingParams['booking_id'] == 0)
		{
			unset($bookingParams['booking_id']);
		}
		$this->setData($bookingParams)->save();
	}
	/**
	* get bkAddressIds
	* @param array $bkAddress
	* return array $bookingIds
	**/
	function getBkAddressIds($address)
	{
		$strAddress = $address['address'];
		$city = trim($address['city']);
		$states = trim($address['states']);
		$strCountry = trim($address['country']);
		$collection = $this->getCollection();
		$arrayFiled = array();
		$arrayValue = array();
		if($strAddress != '')
		{
			/** 
			Author: Maria Imtiaz
			Added column 'booking address' to improve the searching result more on Line no. 153 and 154
			**/
			$arrayFiled[] = 'auto_address';
			$arrayValue[] = array('like'=>'%'.$strAddress.'%');
			$arrayFiled[] = 'booking_address';
			$arrayValue[] = array('like'=>'%'.$strAddress.'%');
		}
		if($city != '')
		{
			$arrayFiled[] =  'booking_city';
			$arrayValue[] = array('like'=>'%'.$city.'%');
		}
		if($states != '')
		{
			$arrayFiled[] =  'booking_state';
			$arrayValue[] = array('like'=>'%'.$states.'%');
		}
		if($city == '' && $states == '' && $strCountry != '')
		{
			$countries = $this->_countryCollection->loadByStore();
			foreach($countries as $country)
			{
				if($country->getName() == $strCountry)
				{
					$arrayFiled[] =  'booking_country';
					$arrayValue[] = array('like'=>'%'.$country->getId().'%');
					break;
				}
			}
		}
		$bookingIds = array();
		if(count($arrayFiled))
		{
			$collection = $this->getCollection();
			$collection->addFieldToFilter($arrayFiled,$arrayValue);
			foreach($collection as $collect)
			{
				$bookingIds[] = $collect->getBookingProductId();
			}
		}
		return $bookingIds;
	}
	/**
	* delete booking system
	* param $bookingId
	* return $this
	**/
	function deleteBkBooking($bookingId)
	{
		$collection = $this->getCollection()
			->addFieldToFilter('booking_product_id',$bookingId);
		$booking = $collection->getFirstItem();
		if($booking)
		{
			$bkBookingId = $booking->getId();
			$this->setId($bkBookingId)->delete();
		}
	}
}