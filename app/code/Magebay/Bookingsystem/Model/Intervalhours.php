<?php
 
namespace Magebay\Bookingsystem\Model;
 
use Magento\Framework\Model\AbstractModel;
use Magebay\Bookingsystem\Model\CalendarsFactory;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\Db;
use Magento\Framework\Data\Collection\AbstractDb;

class Intervalhours extends AbstractModel
{
	protected $_calendarsFactory;
	public function __construct(
		CalendarsFactory $calendarsFactory,
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
		$this->_calendarsFactory = $calendarsFactory;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }
   /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Magebay\Bookingsystem\Model\ResourceModel\Intervalhours');
    }
	function getIntervals($bookingId,$strDate = '')
	{
		$collection = $this->getCollection()
			->addFieldToFilter('intervalhours_booking_id',$bookingId);
		if($strDate != '')
		{
			$collection->addFieldToFilter('intervalhours_check_in',array('lteq'=>$strDate))
					->addFieldToFilter('intervalhours_check_out',array('gteq'=>$strDate));
		}
		$collection->setOrder('intervalhours_id','ASC');
		$dataIntervals = array();
		if(count($collection))
		{
			$dataIntervals = $collection->getData();
		}
		if(!count($dataIntervals) && $strDate != '')
		{
			$collection = $this->getCollection()
				->addFieldToFilter('intervalhours_booking_id',$bookingId);
			$collection->getSelect('intervalhours_check_in IS NULL');
			$collection->setOrder('intervalhours_id','ASC');
				$dataIntervals = $collection->getData();
		}
		return $dataIntervals;
	}
	/*
	* save hours intervals, only for booking_time = 3
	* @param , $bookingId, $params
	* @return $this
	*/
	function saveIntervals($params,$bookingId)
	{
		if($params['booking_time'] == 3)
		{
			//check update intervals
			$okUpdate = false;
			$updatePrice = $params['booking_temp_check_price_update'];
			//get data from calendar
			if($updatePrice == 1)
			{
				$okUpdate = true;
			}
			if($okUpdate)
			{
				//delete old data
				$hoursCollection = $this->getCollection()
						->addFieldToFilter('intervalhours_booking_id',$bookingId);
				$hoursInterIds = array();
				if(count($hoursCollection))
				{
					foreach($hoursCollection as $hourCollection)
					{
						// $this->setId($hourCollection->getId())->delete();
						$hoursInterIds[] = $hourCollection->getId();
					}
				}
				//get data from calendar
				$arrayseletct = array('calendar_startdate','calendar_enddate','calendar_qty');
				$conditions = array('calendar_booking_type'=>'per_day');
				$calendarModel = $this->_calendarsFactory->create();
				$bookingCalendars = $calendarModel->getBkCurrentCalendarsById($bookingId,$arrayseletct,$conditions);
				// \Zend_debug::dump($bookingCalendars->getData());
				
				//add new data
				if(count($bookingCalendars))
				{
					foreach($bookingCalendars as $bookingCalendar)
					{
						$checkIn = $bookingCalendar->getCalendarStartdate();
						$checkOut = $bookingCalendar->getCalendarEnddate();
						$quantity = $bookingCalendar->getCalendarQty();
						$arServiceStart = $params['booking_service_start'];
						$arServiceEnd = $params['booking_service_end'];
						$hourStart = $arServiceStart['type'] == 2 ? ($arServiceStart['hour'] + 12) : $arServiceStart['hour'];
						$hourFinish = $arServiceEnd['type'] == 2 ? ($arServiceEnd['hour'] + 12) : $arServiceEnd['hour'];
						$minuteStart = $arServiceStart['minute'];
						$bookingTimeSlot = $params['booking_time_slot'];
						$bookingTimeBuffer = $params['booking_time_buffer'];
						$totalHour = $hourFinish - $hourStart;
						$tempMinute1 = 0;
						$totalMinute = 0;
						if($arServiceStart['minute'] > 0)
						{
							$tempMinute1 = 60 - $arServiceStart['minute'];
						}
						$tempMinute2 =  $arServiceEnd['minute'];
						if($totalHour > 0)
						{
							if($arServiceStart['minute'] > 0)
							{
								$totalHour--;
							}
							$totalMinute = $totalHour  * 60 + $tempMinute1 + $tempMinute2;
						}
						elseif($totalHour == 0)
						{
							$totalMinute = $arServiceEnd['minute'] - $arServiceStart['minute'];
						}
						if($bookingTimeSlot > 0 && $totalMinute > 0)
						{
							$intervalsHours = $this->createIntervals($totalMinute,$hourStart,$arServiceStart['minute'],$bookingTimeSlot,$bookingTimeBuffer);
							
							if(count($intervalsHours))
							{
								foreach($intervalsHours as $intervalsHour)
								{
									$intervalsHour['start_hour'] = $intervalsHour['start_hour'] > 9 ? $intervalsHour['start_hour'] : '0'.$intervalsHour['start_hour'];
									$intervalsHour['start_minute'] = $intervalsHour['start_minute'] > 9 ? $intervalsHour['start_minute'] : '0'.$intervalsHour['start_minute'];
									$intervalsHour['finish_hour'] = $intervalsHour['finish_hour'] > 9 ? $intervalsHour['finish_hour'] : '0'.$intervalsHour['finish_hour'];
									$intervalsHour['finish_minute'] = $intervalsHour['finish_minute'] > 9 ? $intervalsHour['finish_minute'] : '0'.$intervalsHour['finish_minute'];
									$tempStr = $intervalsHour['start_hour'].'_'.$intervalsHour['start_minute'].'_'.$intervalsHour['finish_hour'].'_'.$intervalsHour['finish_minute'];
									$dataSave = array(
										'intervalhours_booking_id'=>$bookingId,
										'intervalhours_quantity'=>$quantity,
										'intervalhours_booking_time'=>$tempStr,
										'intervalhours_check_in'=>$checkIn,
										'intervalhours_check_out'=>$checkOut,
									);
									// \Zend_debug::dump($dataSave);
									$this->setData($dataSave)->save();
								}
							}
						}
					}
				}
				if(count($hoursInterIds))
				{
					foreach($hoursInterIds as $hoursInterId)
					{
						$this->load($hoursInterId)->delete();
					}
				}
				//exit();
			}
		}
	}
	function getInervalsQty($bookingId,$strDate,$strInterTime)
	{
		$interval = null;
		if(trim($strInterTime) != '')
		{
			$collection = $this->getCollection()
				->addFieldToFilter('intervalhours_booking_id',$bookingId)
				->addFieldToFilter('intervalhours_booking_time',$strInterTime)
				->addFieldToFilter('intervalhours_check_in',array('lteq'=>$strDate))
				->addFieldToFilter('intervalhours_check_out',array('gteq'=>$strDate));
			$collect = $collection->getFirstItem();
			if($collect->getId())
			{
				$interval = $collect;
			}
			else
			{
				$collection = $this->getCollection()
					->addFieldToFilter('intervalhours_booking_id',$bookingId)
					->addFieldToFilter('intervalhours_booking_time',$strInterTime);
				$collection->getSelect()->where('intervalhours_check_in IS NULL');
					$collect = $collection->getFirstItem();
					$interval = $collect;
			}
		}
		return $interval;
	}
	function createIntervals($totalMinute,$hourStart,$minuteStart,$bookingTimeSlot,$bookingTimeBuffer)
	{
		$arrayIntervals = array();
		$i = 0;
		$tempHour = $hourStart;
		$tempMinute = $minuteStart;
		while($totalMinute > 0)
		{
			if($i > 0)
			{
				$tempMinute = $tempMinute + $bookingTimeBuffer;
				if($tempMinute >= 60)
				{
					$mHour = floor($tempMinute / 60);
					$tempMinute = $tempMinute - ($mHour * 60);
					$tempHour += $mHour;
				}
			}
			if($totalMinute < $bookingTimeSlot)
			{
				break;
			}
			$arrayIntervals[$i]['start_hour'] = $tempHour;
			$arrayIntervals[$i]['start_minute'] = $tempMinute;
			$tempMinute = $tempMinute + $bookingTimeSlot;
			if($tempMinute >= 60)
			{
				$mHour = floor($tempMinute / 60);
				$tempMinute = $tempMinute - ($mHour * 60);
				$tempHour += $mHour;
			}
			$arrayIntervals[$i]['finish_hour'] = $tempHour;
			$arrayIntervals[$i]['finish_minute'] = $tempMinute;
			$totalMinute = $totalMinute - ($bookingTimeSlot + $bookingTimeBuffer);
			$i++;
		}
		return $arrayIntervals;
	}
	function deleteIntervalsHours($bookingId)
	{
		$collection = $this->getCollection()
			->addFieldToFilter('intervalhours_booking_id',$bookingId);
		if(count($collection))
		{
			foreach($collection as $collect)
			{
				$this->setId($collect->getId())->delete();
			}
		}
	}
}