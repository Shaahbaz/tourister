<?php
 
namespace Magebay\Bookingsystem\Helper;
 
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magebay\Bookingsystem\Model\BookingsFactory;
use Magebay\Bookingsystem\Helper\BkHelperDate;
use Magebay\Bookingsystem\Helper\BkSimplePriceHelper;
use Magebay\Bookingsystem\Helper\IntervalsPrice;
use Magebay\Bookingsystem\Model\OptionsFactory;
use Magebay\Bookingsystem\Model\RoomsFactory;
use Magebay\Bookingsystem\Model\RoomtypesFactory;
use Magebay\Bookingsystem\Model\OptionsdropdownFactory;
use Magebay\Bookingsystem\Helper\BkText;
use Magebay\Bookingsystem\Helper\RoomPrice;

class BkCustomOptions extends AbstractHelper
{
	/**
     * @var \Magebay\Bookingsystem\Model\Bookings
    */
	protected $_bookingFactory;
	/**
     * @var \Magebay\Bookingsystem\Helper\BkHelperDate
    */
	protected $_bkHelperDate;
	/** Helper
     *
     * @var \Magebay\Bookingsystem\Helper\BkSimplePriceHelper
    **/
	protected $_bkSimplePriceHelper;
	/** Helper
     *
     * @var \Magebay\Bookingsystem\Helper\IntervalsPrice
    **/
	protected $_intervalsPrice;
	/**
     * @var \Magebay\Bookingsystem\Model\OptionsFactory
    */
	protected $_optionsFactory;
	/**
     * @var \Magebay\Bookingsystem\Model\RoomsFactory
    */
	protected $_roomsFactory;
	/**
     * @var \Magebay\Bookingsystem\Model\RoomtypesFactory
    */
	protected $_roomtypesFactory;
	/**
     * @var \Magebay\Bookingsystem\Model\OptionsdropdownFactory
    */
	protected $_optionsdropdownFactory;
	/**
     * @var \Magebay\Bookingsystem\Helper\BkText
    */
	protected $_bkText;
	/**
     * @var \Magebay\Bookingsystem\Helper\RoomPrice
    */
	protected $_roomPrice;
	public function __construct(
       Context $context,
	   BookingsFactory $bookingFactory,
	   BkHelperDate $bkHelperDate,
	   BkSimplePriceHelper $bkSimplePriceHelper,
	   IntervalsPrice $intervalsPrice,
	   OptionsFactory $optionsFactory,
	   RoomsFactory $roomsFactory,
	   RoomtypesFactory $roomtypesFactory,
	   OptionsdropdownFactory $optionsdropdownFactory,
	   BkText $bkText,
	   RoomPrice $roomPrice
    ) 
	{
		parent::__construct($context);
		$this->_bookingFactory = $bookingFactory;
		$this->_bkHelperDate = $bkHelperDate;
		$this->_bkSimplePriceHelper = $bkSimplePriceHelper;
		$this->_intervalsPrice = $intervalsPrice;
		$this->_optionsFactory = $optionsFactory;
		$this->_roomsFactory = $roomsFactory;
		$this->_roomtypesFactory = $roomtypesFactory;
		$this->_optionsdropdownFactory = $optionsdropdownFactory;
		$this->_bkText = $bkText;
		$this->_roomPrice = $roomPrice;
    }
	function createExtractOptions($product,$params)
	{
		$additionalOptions = array();
		$enable = $this->_bkHelperDate->getFieldSetting('bookingsystem/setting/enable');
		$formatDate = $this->_bkHelperDate->getFieldSetting('bookingsystem/setting/format_date');
		$typeTime = $this->_bkHelperDate->getFieldSetting('bookingsystem/setting/time_mode');
		$storeId = $this->_bkText->getbkCurrentStore();
		$okAddCart = true;
		$qty = isset($params['qty']) ? $params['qty'] : 1;
		$oldOrderItemId = 0;
		if($enable == 1)
		{
			$bookingModel = $this->_bookingFactory->create();
			//check booking
			$booking = $bookingModel->getBooking($product->getId());
			$oldOrderItemId  = isset($params['bk_order_item_id']) ? $params['bk_order_item_id'] : 0;
			if($booking && $booking->getId() && $booking->getTypeId() == 'booking' && isset($params['check_in']))
			{
				$itemId = isset($params['backend_item_id']) ? $params['backend_item_id'] : 0;
				//simple
				if($booking->getBookingType() == 'per_day')
				{
	
					$arPrice = array();
					if($booking->getBookingTime() == 1 || $booking->getBookingTime() == 2)
					{
						//validate time
						if(!$this->_bkHelperDate->validateBkDate($params['check_in'],$formatDate) || !$this->_bkHelperDate->validateBkDate($params['check_out'],$formatDate))
						{
							$okAddCart = false;
						}
						else
						{
							$additionalOptions[] = array(
							'label' => __('Check In'),
							 'value' => $params['check_in'],
							);
							$additionalOptions[] = array(
								'label' => __('Check Out'),
								'value' => $params['check_out'],
							);
							$checkIn = $this->_bkHelperDate->convertFormatDate($params['check_in']);
							$checkOut = $this->_bkHelperDate->convertFormatDate($params['check_out']);
							if($booking->getBookingTime() == 1)
							{
								$arPrice = $this->_bkSimplePriceHelper->getPriceBetweenDays($booking,$checkIn,$checkOut,$qty,$itemId,array(),$oldOrderItemId);
								if($arPrice['str_error'] != '')
								{
									$okAddCart = false;
								}
								$additionalOptions[] = array(
									'label' => __('Total Days'),
									'value' => $arPrice['total_days'],
								);
							}
							elseif($booking->getBookingTime() == 2)
							{
								$m1 = $params['from_time_m'] > 10 ? $params['from_time_m'] : '0'.$params['from_time_m'];
								$m2 = $params['to_time_m'] > 10 ? $params['to_time_m'] : '0'.$params['to_time_m'];
								$timeType1 = $params['from_time_t'] == 1 ? __('AM') : __('PM');
								$timeType2 = $params['to_time_t'] == 1 ? __('AM') : __('PM');
								$fromHour =  $params['from_time_t'] == 1 ? $params['from_time_h'] : ($params['from_time_h'] + 12);
								$toHour =  $params['to_time_t'] == 1 ? $params['to_time_h'] : ($params['to_time_h'] + 12);
								$textTime1 = $params['from_time_h'].' : '.$m1.' : '.$timeType1;
								if($typeTime == 2)
								{
									$textTime1 = $params['from_time_h'].' : '.$m1;
								}
								$textTime2 = $params['to_time_h'] .' : '.$m2.' : '.$timeType2;
								if($typeTime == 2)
								{
									$textTime2 = $params['to_time_h'] .' : '.$m2;
								}
								$additionalOptions[] = array(
									'label' => __('Service Start'),
									 'value' => $textTime1
									);
								$additionalOptions[] = array(
									'label' => __('Service End'),
									 'value' => $textTime2
									);
								$arPrice = $this->_bkSimplePriceHelper->getHourPriceBetweenDays($booking,$checkIn,$checkOut,$fromHour,$toHour,$params['from_time_m'],$params['to_time_m'],$qty,$itemId,array(),$oldOrderItemId);
								if($arPrice['str_error'] != '')
								{
									$okAddCart = false;
								}
								$additionalOptions[] = array(
									'label' => __('Total Days'),
									'value' => $arPrice['total_days'],
								);
								$additionalOptions[] = array(
									'label' => __('Total Hours'),
									 'value' => $arPrice['total_hours'],
									);
							}
						}
					}
					elseif($booking->getBookingTime() == 3)
					{
						$intervalsHours = isset($params['intervals_hours']) ? $params['intervals_hours'] : array();
						if(!$this->_bkHelperDate->validateBkDate($params['check_in'],$formatDate) || !count(($intervalsHours)))
						{
							$okAddCart = false;
						}
						else
						{
							$checkIn = $this->_bkHelperDate->convertFormatDate($params['check_in']);
							$arPrice = $this->_intervalsPrice->getIntervalsHoursPrice($booking,$checkIn,$qty,$intervalsHours,$itemId,array(),$oldOrderItemId);
							if($arPrice['str_error'] != '')
							{
								$okAddCart = false;
							}
							else
							{
								$strIntervals = '';
								$i = 0;
								foreach($intervalsHours as $intervalsHour)
								{
									$tempIntervals = '';
									$arIntervalsHours = explode('_',$intervalsHour);
									$typeTime1 = __('AM');
									$typeTime2 = __('AM');
									if($arIntervalsHours[0] > 12)
									{
										$arIntervalsHours[0] = $arIntervalsHours[0] - 12;
										$typeTime1 = __('PM');
									}
									if($arIntervalsHours[2] > 12)
									{
										$arIntervalsHours[2] = $arIntervalsHours[2] - 12;
										$typeTime2 = __('PM');
									}
									$tempIntervals = $arIntervalsHours[0].':'.$arIntervalsHours[1].' '. $typeTime1;
									if($booking->getShowTimeFinish() == 1)
									{
										$tempIntervals .= ' - '.$arIntervalsHours[2].':'.$arIntervalsHours[3].' '. $typeTime2;
									}
									
									if($i == 0)
									{
										$strIntervals = $tempIntervals;
									}
									else
									{
										$strIntervals .= ', '.$tempIntervals;
									}
									$i++;
								}
								$additionalOptions[] = array(
									'label' => __('Check In'),
									'value' => $params['check_in'],
								);
								$additionalOptions[] = array(
									'label' => __('Intervals'),
									 'value' => $strIntervals,
									);
							}
							
						}
					}
				}
				//booking for hotel
				elseif($booking->getBookingType() == 'hotel')
				{
					$checkIn = $params['room_check_in'];
					$checkOut = $params['room_check_out'];
					$roomId = $params['room_id'];
					$roomModel = $this->_roomsFactory->create();
					$room = $roomModel->load($roomId);
					if($room)
					{
						if(!$this->_bkHelperDate->validateBkDate($checkIn,$formatDate) || !$this->_bkHelperDate->validateBkDate($checkOut,$formatDate))
						{
							$okAddCart = false;
						}
						//get title room type
						$roomTypeModel = $this->_roomtypesFactory->create();
						$roomType = $roomTypeModel->load($room->getRoomType());
						$roomTitle =  $this->_bkText->showTranslateText($roomType->getRoomtypeTitle(),$roomType->getRoomtypeTitleTransalte());
						$additionalOptions[] = array(
							'label' => __('Room Type'),
							'value' => $roomTitle,
						);
						$additionalOptions[] = array(
							'label' => __('Check In'),
							'value' => $checkIn,
						);
						$additionalOptions[] = array(
							'label' => __('Check Out'),
							'value' => $checkOut,
						);
						$checkIn = $this->_bkHelperDate->convertFormatDate($checkIn);
						$checkOut = $this->_bkHelperDate->convertFormatDate($checkOut);
						$arPrice = $this->_roomPrice->getPriceBetweenDays($room,$checkIn,$checkOut,$qty,$itemId,array(),$oldOrderItemId);
						if($arPrice['str_error'] != '')
						{
							$okAddCart = false;
						}
						$additionalOptions[] = array(
								'label' => __('Total Days'),
								'value' => $arPrice['total_days'],
						);
					}
				}
				if($okAddCart)
				{
					if(isset($params['addons']))
					{
						if(count($params['addons']))
						{
							$addonKeyExit = array();
							foreach($params['addons'] as $key => $addon)
							{
								$addonsModel = $this->_optionsFactory->create();
								$addonsOptions = $addonsModel->load($key);
								if($addonsOptions->getId())
								{
									$titleAddons = $this->_bkText->showTranslateText($addonsOptions->getOptionTitle(),$addonsOptions->getOptionTitleTranslate());
									if($addonsOptions->getOptionType() == 1)
									{
										$additionalOptions[] = array(
											'label' => $titleAddons,
											'value' => $addon
										);
									}
									elseif($addonsOptions->getOptionType() == 2 || $addonsOptions->getOptionType() == 4)
									{
										$optionSelectModel = $this->_optionsdropdownFactory->create();
										$valueRows = $optionSelectModel->getBkValueOptions($key);
										$strTitleRow = '';
										if(count($valueRows))
										{
											foreach($valueRows as $valueRow)
											{
												if($valueRow->getDropdownPrice() == $addon)
												{
													$strTitleRow = $this->_bkText->showTranslateText($valueRow->getDropdownTitle(),$valueRow->getDropdownTitleTranslate());
													$additionalOptions[] = array(
														'label' => $titleAddons,
														'value' => $strTitleRow
														);
													break;
												}
											}
										}
									}
									elseif($addonsOptions->getOptionType() == 3 || $addonsOptions->getOptionType() == 5)
									{
										$optionSelectModel = $this->_optionsdropdownFactory->create();
										$valueRows = $optionSelectModel->getBkValueOptions($key);
										$strTitleRow = '';
										if(count($valueRows))
										{
											foreach($valueRows as $valueRow)
											{
												
												if(count($addon))
												{
													foreach($addon as $mAddon)
													{
														if($mAddon == $valueRow->getDropdownPrice())
														{
															if($strTitleRow == '')
															{
																$strTitleRow .= $this->_bkText->showTranslateText($valueRow->getDropdownTitle(),$valueRow->getDropdownTitleTranslate());
															}
															else
															{
																$strTitleRow .= ', '.$this->_bkText->showTranslateText($valueRow->getDropdownTitle(),$valueRow->getDropdownTitleTranslate());
															}
														}
													}
													
												}
											}
											if($strTitleRow != '')
											{
												$additionalOptions[] = array(
												'label' => $titleAddons,
												'value' => $strTitleRow
												);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} 
		return array(
			'status'=>$okAddCart,
			'bk_options'=>$additionalOptions
		);	
	}
	function addBkOptions($product,$params)
	{
		$bkData = $this->createExtractOptions($product,$params);
		if($bkData['status'] == true)
		{
			$additionalOptions = $bkData['bk_options'];
			$product->addCustomOption('additional_options', serialize($additionalOptions));
		}
		else
		{
			throw new \Exception(__('Dates are not available. Please check again!'));
		} 
	}
}