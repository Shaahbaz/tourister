var config = {
		map: {
		'*': {
			bk_bootstrap_min : 'Magebay_Bookingsystem/js/bootstrap.min',
			bk_owl_carousel : 'Magebay_Bookingsystem/js/owl.carousel',
			bk_calendar_simple : 'Magebay_Bookingsystem/js/calendar',
			bk_calendar_intervals : 'Magebay_Bookingsystem/js/calendar_intervals',
			bk_calendar_room : 'Magebay_Bookingsystem/js/room_calendar',
			bk_map_point : 'Magebay_Bookingsystem/js/map_point',
			'Magento_Catalog/js/catalog-add-to-cart' : 'Magebay_Bookingsystem/js/catalog-add-to-cart',
			}
		}
};