var config = {
		map: {
		'*': {
			bk_calendar : 'Magebay_Bookingsystem/js/calendar',
			google_map_lib : 'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyAliGaYxyjlaO0WBDFPsxb3qXxGlT9xC2s',
			bk_map_js : 'Magebay_Bookingsystem/js/bk_maps',
			bk_moment_min : 'Magebay_Bookingsystem/js/moment.min',
			bk_fullcalendar_min : 'Magebay_Bookingsystem/js/fullcalendar.min',
			bk_calendar_simple_order : 'Magebay_Bookingsystem/js/order/calendar_simple',
			bk_calendar_intervals_order : 'Magebay_Bookingsystem/js/order/calendar_intervals',
			bk_calendar_rooms_order : 'Magebay_Bookingsystem/js/order/calendar_rooms',
			}
		},
		shim: {
			'bk_map_js' : {
				'deps': ['google_map_lib']
			},
		}
};