<?php
 
namespace Magebay\Bookingsystem\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class Search extends \Magento\Framework\App\Action\Action
{
	 /**
     * Result page factory
     *
     * @var \Magento\Framework\Controller\Result\JsonFactory;
     */
	protected $_resultJsonFactory;
    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
    */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
		JsonFactory $resultJsonFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
		$this->_resultJsonFactory = $resultJsonFactory;
    }
    public function execute()
    {
        $resultJson = $this->_resultJsonFactory->create();
		// $this->_view->getLayout()->createBlock('Magebay\Bookingsystem\Block\Search')->getBkDataBookings();
		$htmlItems = $this->_view->getLayout()->createBlock('Magebay\Bookingsystem\Block\Search')->setTemplate('Magebay_Bookingsystem::search/result-search.phtml')->toHtml();
		$htmlSidebar = $this->_view->getLayout()->createBlock('Magebay\Bookingsystem\Block\Search')->setTemplate('Magebay_Bookingsystem::search/sidebar.phtml')->toHtml();
		$response = array('html_items'=> $htmlItems,'html_sidebar'=>$htmlSidebar);
		return $resultJson->setData($response);
    }
}
 