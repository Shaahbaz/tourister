<?php
 
namespace Magebay\Bookingsystem\Controller\Adminhtml\Booking;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magebay\Bookingsystem\Model\CalendarsFactory;

class Calendars extends Action
{
	 /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;
    /**
     * Result page factory
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
	 
    protected $_resultPageFactory;
	 /**
     * Result page factory
     *
     * @var \Magento\Framework\Controller\Result\JsonFactory;
     */
	protected $_resultJsonFactory;
	
	 /**
     * Result page factory
     *
     * @var \Magebay\Bookingsystem\Model\CalendarsFactory;
     */
	 protected $_calendarsFactory;
	function __construct
	(
		Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
		JsonFactory $resultJsonFactory,
		CalendarsFactory $calendarsFactory
	)
	{
		parent::__construct($context);
		$this->_coreRegistry = $coreRegistry;
        $this->_resultPageFactory = $resultPageFactory;
		$this->_resultJsonFactory = $resultJsonFactory;
		$this->_calendarsFactory = $calendarsFactory;
	}
	public function execute()
	{
		$bookingId = $this->_request->getParam('booking_id',0);
		$bookingType = $this->_request->getParam('booking_type','per_day');
		$conditions = array('calendar_booking_type'=>$bookingType);
		$model = $this->_calendarsFactory->create();
		$calendars = $model->getBkCurrentCalendarsById($bookingId,array('*'),$conditions);
		$dataCalendar = array();
		if(count($calendars))
		{
			foreach($calendars as $key => $calendar)
			{
				$dataCalendar[$key]['start_date'] = $calendar->getCalendarStartdate();
				$dataCalendar[$key]['end_date'] = $calendar->getCalendarEnddate();
				$dataCalendar[$key]['status'] = $calendar->getCalendarStatus();
				$dataCalendar[$key]['price'] = $calendar->getCalendarPrice();
				$dataCalendar[$key]['promo'] = $calendar->getCalendarPromo();
				$dataCalendar[$key]['qty'] = $calendar->getCalendarQty();
				$dataCalendar[$key]['default_value'] = $calendar->getCalendarDefaultValue();
			}
		}
		if(count($dataCalendar))
		{
			$arCalendars = $dataCalendar;
			$arDefaultCalendar = array();
			foreach($arCalendars as $key => $arCalendar)
			{
				if($arCalendar['default_value'] == 1)
				{
					$arDefaultCalendar = $arCalendar;
					unset($arCalendars[$key]);
					break;
				}
			}
			if(count($arDefaultCalendar))
			{
				$arCalendars[] = $arDefaultCalendar; 
			}
			$dataCalendar = array_values($arCalendars);
		}
		$resultJson = $this->_resultJsonFactory->create();
		return $resultJson->setData($dataCalendar);
		
	}
	protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magebay_Bookingsystem::update_booking');
    }
}