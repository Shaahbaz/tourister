<?php 

namespace Magebay\Bookingsystem\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
class UpgradeSchema implements  UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context){
        $setup->startSetup();
        if (version_compare($context->getVersion(), '2.0.1') < 0) {
            // Get module table
            $tableAddonOptoin = $setup->getTable('booking_options');
            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableAddonOptoin) == true) {
                // Declare data
                $columns = [
                    'option_price_type' => [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'nullable' => true,
                        'comment' => 'option price type',
                    ],
                ];
                $connection = $setup->getConnection();
                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableAddonOptoin, $name, $definition);
                }

            }
        }
		if (version_compare($context->getVersion(), '2.0.1') < 0) {
			$bookingAtc = $setup->getTable('booking_act');
			if ($setup->getConnection()->isTableExists($bookingAtc) != true) {
				$tableBookingAtc = $setup->getConnection()
					->newTable($bookingAtc)
					 ->addColumn(
                'act_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Id'
            )
            ->addColumn(
                'domain_count',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Domain Count'
            )
            ->addColumn(
                'domain_list',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'No comment'
            )
            ->addColumn(
                'path',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'No comment'
            )
            ->addColumn(
                'extension_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'No comment'
            )
            ->addColumn(
                'act_key',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'No comment'
            )
            ->addColumn(
                'domains',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                500,
                ['nullable' => false],
                'No comment'
            )
            ->addColumn(
                'created_time',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATE,
                null,
                ['nullable' => false],
                'No comment'
            )
            ->addColumn(
                'is_valid',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['nullable' => false, 'default' => 0],
                'No comment'
            )
			->setComment('Booking ACT')
			->setOption('type', 'InnoDB')
			->setOption('charset', 'utf8');
				$setup->getConnection()->createTable($tableBookingAtc);
			}
		}
        $setup->endSetup();
    }
}