<?php

namespace Esparksinc\BulkCmsImportExport\Api;

interface ContentInterface
{
    const CMS_MODE_UPDATE = 'update';
    const CMS_MODE_SKIP = 'skip';

    const MEDIA_MODE_NONE = 'none';
    const MEDIA_MODE_UPDATE = 'update';
    const MEDIA_MODE_SKIP = 'skip';

    /**
     * This is for setting stores mapping on import
     * @param array $storesMap
     * @return ContentInterface
     */
    public function setStoresMap(array $storesMap): ContentInterface;

    /**
     * This will return single CMS block to array
     * @param \Magento\Cms\Api\Data\BlockInterface $blockInterface
     * @return array
     */
    public function blockToArray(\Magento\Cms\Api\Data\BlockInterface $blockInterface): array;

    /**
     * This will return single CMS page to array
     * @param \Magento\Cms\Api\Data\PageInterface $pageInterface
     * @return array
     */
    public function pageToArray(\Magento\Cms\Api\Data\PageInterface $pageInterface): array;

    /**
     * This will return multiple CMS blocks as array
     * @param \Magento\Cms\Api\Data\BlockInterface[] $blockInterfaces
     * @return array
     */
    public function blocksToArray(array $blockInterfaces): array;

    /**
     * This will return multiple CMS pages as array
     * @param \Magento\Cms\Api\Data\PageInterface[] $pageInterfaces
     * @return array
     */
    public function pagesToArray(array $pageInterfaces): array;

    /**
     * This will create a zip file and return it as a name
     * @param \Magento\Cms\Api\Data\PageInterface[] $pageInterfaces
     * @param \Magento\Cms\Api\Data\BlockInterface[] $blockInterfaces
     * @return string
     */
    public function asZipFile(array $pageInterfaces, array $blockInterfaces): string;

    /**
     * This will return single page from array and return true or false as boolean
     * @param array $pageData
     * @return bool
     */
    public function importPageFromArray(array $pageData): bool;

    /**
     * This will return single block from array and return true or false as boolean
     * @param array $blockData
     * @return bool
     */
    public function importBlockFromArray(array $blockData): bool;

    /**
     * This will get contents of array and return the number of array and will return its count
     * @param array $payload
     * @param string $archivePath = null
     * @return int
     */
    public function importFromArray(array $payload, $archivePath = null): int;

    /**
     * This will return contents of zip and return it count
     * @param string $fileName
     * @param bool $rm = true
     * @return int
     */
    public function importFromZipFile($fileName, $rm = false): int;
}
