<?php

namespace Esparksinc\BulkCmsImportExport\Block\Adminhtml\Import\Upload;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Store\Api\StoreRepositoryInterface;
use Esparksinc\BulkCmsImportExport\Api\ContentInterface;

class Form extends Generic
{
    protected $mediaMode;
    protected $cmsMode;
    protected $storeRepositoryInterface;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        StoreRepositoryInterface $storeRepositoryInterface,
        array $data = []
    ) {
        $this->storeRepositoryInterface = $storeRepositoryInterface;

        parent::__construct($context, $registry, $formFactory, $data);
    }

    protected function _prepareForm()
    {
        
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'edit_form',
                    'enctype' => 'multipart/form-data',
                    'action' => $this->getUrl('*/*/post'),
                    'method' => 'post'
                ]
            ]
        );

        $form->setHtmlIdPrefix('import');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            [
                'legend' => __('Upload Cms File'),
                'class' => 'fieldset-wide',
            ]
        );


        $fieldset->addField(
            'zip',
            'file',
            [
                'name' => 'zip',
                'label' => __('Click Browse to Select File'),
                'title' => __('Click Browse to Select File'),
                'required' => true,
            ]
        );
       
        $fieldset = $form->addFieldset(
            'alert',
            [
                'legend' => __('*Warning: On import all existing default cms page(s)/block(s) will also be updated. If you dont want to import default pages(s)/block(s) then only export required cms page(s)/block(s)'),
                'class' => 'alert alert-danger',
            ]
        );
        

        $stores = $this->storeRepositoryInterface->getList();
        
        $values = [];
        foreach ($stores as $storeInterface) {
            $values['store_map:'.$storeInterface->getCode()] = $storeInterface->getCode();
        }

        $form->setValues($values); // setting default values

        $form->setUseContainer(true);
        $this->setForm($form);


        return parent::_prepareForm();
    }
}
