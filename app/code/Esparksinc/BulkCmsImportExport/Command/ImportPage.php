<?php

namespace Esparksinc\BulkCmsImportExport\Command;

use Magento\Framework\ObjectManagerInterface;
use Esparksinc\BulkCmsImportExport\Api\ContentInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportPage extends Command
{

    protected $pageInterface;
    private $objectManager;

    
    public function __construct(
        ObjectManagerInterface $objectManager
    ) {
        $this->objectManager = $objectManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('cms:import');
        $this->setDescription('Import CMS');
        $this->addArgument('zip', InputArgument::REQUIRED, __('This contains Cms details'));

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $contentInterface = $this->objectManager->get(ContentInterface::class);

        $zipFile = $input->getArgument('zipfile');
        if ($contentInterface->importFromZipFile($zipFile, false) == 0) {
            throw new \Exception(__('Empty'));
        }

        $output->writeln('Done.');
    }
}
