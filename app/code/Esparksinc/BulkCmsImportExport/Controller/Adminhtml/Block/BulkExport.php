<?php

namespace Esparksinc\BulkCmsImportExport\Controller\Adminhtml\Block;

use Magento\Backend\App\Response\Http\FileFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Backend\App\Action;
use Magento\Cms\Model\ResourceModel\Block\CollectionFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Esparksinc\BulkCmsImportExport\Api\ContentInterface as ImportExportContentInterface;

class BulkExport extends Action
{
    protected $filter;
    protected $collectionFactory;
    protected $importExportContentInterface;
    protected $fileFactory;
    protected $dateTime;


    public function __construct(
        Action\Context $context,
        Filter $filter,
        ImportExportContentInterface $importExportContentInterface,
        CollectionFactory $collectionFactory,
        FileFactory $fileFactory,
        DateTime $dateTime
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->importExportContentInterface = $importExportContentInterface;
        $this->fileFactory = $fileFactory;
        $this->dateTime = $dateTime;

        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Esparksinc_BulkCmsImportExport::export_block');
    }

    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());

        $pages = [];
        foreach ($collection as $page) {
            $pages[] = $page;
        }

        return $this->fileFactory->create(
            sprintf('CMS_Block(s)_%s.zip', $this->dateTime->date('d-m-Y')),
            [
                'type' => 'filename',
                'value' => $this->importExportContentInterface->asZipFile([], $pages),
                'rm' => true,
            ],
            DirectoryList::VAR_DIR,
            'application/zip'
        );
    }
}
