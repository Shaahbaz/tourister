<?php

namespace Esparksinc\BulkCmsImportExport\Controller\Adminhtml\Import;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\File\UploaderFactory;
use Esparksinc\BulkCmsImportExport\Api\ContentInterface;
use Esparksinc\BulkCmsImportExport\Model\Filesystem;

use Magento\Framework\Message\ManagerInterface;

class Post extends Action
{
    protected $uploaderFactory;
    protected $contentInterface;
    protected $filesystem;
    protected $redirectFactory;

    protected $messageManager;


    public function __construct(
        Action\Context $context,
        UploaderFactory $uploaderFactory,
        ContentInterface $contentInterface,
        RedirectFactory $redirectFactory,
        Filesystem $filesystem,
        ManagerInterface $messageManager
    ) {

        $this->messageManager = $messageManager;

        $this->uploaderFactory = $uploaderFactory;
        $this->contentInterface = $contentInterface;
        $this->filesystem = $filesystem;
        $this->redirectFactory = $redirectFactory;

        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Esparksinc_BulkCmsImportExport::import');
    }

    public function execute()
    {
        
        // $this->messageManager->addWarning(__("Warning"));


        $destinationPath = $this->filesystem->getUploadPath();

        $uploader = $this->uploaderFactory->create(['fileId' => 'zip']);
        $uploader->setAllowRenameFiles(true);
        $uploader->setFilesDispersion(true);
        $uploader->setAllowCreateFolders(true);
        $result = $uploader->save($destinationPath);

        $zipFile = $result['path'].$result['file'];

        
        $count = $this->contentInterface->importFromZipFile($zipFile, true);

        $this->messageManager->addSuccess(__('Cms Page(s)/Block(s) successfully imported'));
        

        $resultRedirect = $this->redirectFactory->create();
        return $resultRedirect->setPath('*/*/index');
    }
}
