<?php

namespace Esparksinc\BulkCmsImportExport\Model;

use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Framework\Json\DecoderInterface;
use Magento\Framework\Json\EncoderInterface;
use Magento\Cms\Api\Data\PageInterface as PageInterface;
use Magento\Cms\Api\Data\BlockInterface as BlockInterface;
use Magento\Cms\Model\BlockFactory as BlockFactory;
use Magento\Cms\Model\PageFactory as PageFactory;
use Magento\Cms\Model\ResourceModel\Page\CollectionFactory as PageCollectionFactory;
use Magento\Cms\Model\ResourceModel\Block\CollectionFactory as BlockCollectionFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Esparksinc\BulkCmsImportExport\Api\ContentInterface;
use Magento\Framework\Filesystem\Io\File;

class Content implements ContentInterface
{
    const JSON_FILENAME = 'cms.json';
    const MEDIA_ARCHIVE_PATH = 'media';

    protected $storeRepositoryInterface;
    protected $encoderInterface;
    protected $decoderInterface;
    protected $pageCollectionFactory;
    protected $blockCollectionFactory;
    protected $blockRepositoryInterface;
    protected $pageFactory;
    protected $blockFactory;
    protected $filesystem;
    protected $file;
    protected $dateTime;

    public function __construct(
        StoreRepositoryInterface $storeRepositoryInterface,
        EncoderInterface $encoderInterface,
        DecoderInterface $decoderInterface,
        PageFactory $pageFactory,
        PageCollectionFactory $pageCollectionFactory,
        BlockFactory $blockFactory,
        BlockCollectionFactory $blockCollectionFactory,
        BlockRepositoryInterface $blockRepositoryInterface,
        Filesystem $filesystem,
        File $file,
        DateTime $dateTime
    ) {
        $this->storeRepositoryInterface = $storeRepositoryInterface;
        $this->encoderInterface = $encoderInterface;
        $this->decoderInterface = $decoderInterface;
        $this->pageCollectionFactory = $pageCollectionFactory;
        $this->pageFactory = $pageFactory;
        $this->blockCollectionFactory = $blockCollectionFactory;
        $this->blockFactory = $blockFactory;
        $this->blockRepositoryInterface = $blockRepositoryInterface;
        $this->filesystem = $filesystem;
        $this->file = $file;
        $this->dateTime = $dateTime;

        $this->storesMap = [];
        $stores = $this->storeRepositoryInterface->getList();
        foreach ($stores as $store) {
            $this->storesMap[$store->getCode()] = $store->getCode();
        }
    }

    
    public function asZipFile(array $pageInterfaces, array $blockInterfaces): string //this function will create a zip file and return the file name
    {
        $pagesArray = $this->pagesToArray($pageInterfaces);
        $blocksArray = $this->blocksToArray($blockInterfaces);

        $contentArray = array_merge_recursive($pagesArray, $blocksArray);

        $jsonPayload = $this->encoderInterface->encode($contentArray);

        $exportPath = $this->filesystem->getExportPath();

        $zipFile = $exportPath . '/' . sprintf('cms%s.zip', $this->dateTime->date('d-m-Y'));
        $relativeZipFile = Filesystem::EXPORT_PATH . '/' . sprintf('cms%s.zip', $this->dateTime->date('d-m-Y'));

        $zipArchive = new \ZipArchive();
        $zipArchive->open($zipFile, \ZipArchive::CREATE);

        // Add pages json
        $zipArchive->addFromString(self::JSON_FILENAME, $jsonPayload);

        // Add media files
        foreach ($contentArray['media'] as $mediaFile) {
            $absMediaPath = $this->filesystem->getMediaPath($mediaFile);
            if ($this->file->fileExists($absMediaPath, true)) {
                $zipArchive->addFile($absMediaPath, self::MEDIA_ARCHIVE_PATH . '/' . $mediaFile);
            }
        }

        $zipArchive->close();
        $this->file->rm($exportPath, true);

        return $relativeZipFile;
    }

    public function pagesToArray(array $pageInterfaces): array //seting and returning multiple cms pages in the form of array
    {
        $pages = [];
        $media = [];

        foreach ($pageInterfaces as $pageInterface) {
            $pageInfo = $this->pageToArray($pageInterface);
            $pages[$this->_getPageKey($pageInterface)] = $pageInfo;
            $media = array_merge($media, $pageInfo['media']);
        }

        return [
            'pages' => $pages,
            'media' => $media,
        ];
    }

    
    public function pageToArray(PageInterface $pageInterface): array //seting and returning single cms page in the form of array
    {
        // Extract attachments
        $media = $this->getMediaAttachments($pageInterface->getContent());

        $payload = [
            'cms' => [
                PageInterface::IDENTIFIER => $pageInterface->getIdentifier(),
                PageInterface::TITLE => $pageInterface->getTitle(),
                PageInterface::PAGE_LAYOUT => $pageInterface->getPageLayout(),
                PageInterface::META_KEYWORDS => $pageInterface->getMetaKeywords(),
                PageInterface::META_DESCRIPTION => $pageInterface->getMetaDescription(),
                PageInterface::CONTENT_HEADING => $pageInterface->getContentHeading(),
                PageInterface::CONTENT => $pageInterface->getContent(),
                PageInterface::SORT_ORDER => $pageInterface->getSortOrder(),
                PageInterface::LAYOUT_UPDATE_XML => $pageInterface->getLayoutUpdateXml(),
                PageInterface::CUSTOM_THEME => $pageInterface->getCustomTheme(),
                PageInterface::CUSTOM_ROOT_TEMPLATE => $pageInterface->getCustomRootTemplate(),
                PageInterface::CUSTOM_LAYOUT_UPDATE_XML => $pageInterface->getCustomLayoutUpdateXml(),
                PageInterface::CUSTOM_THEME_FROM => $pageInterface->getCustomThemeFrom(),
                PageInterface::CUSTOM_THEME_TO => $pageInterface->getCustomThemeTo(),
                PageInterface::IS_ACTIVE => $pageInterface->isActive(),
            ],
            'stores' => $this->getStoreCodes($pageInterface->getStoreId()),
            'media' => $media,
        ];

        return $payload;
    }

    public function getMediaAttachments($content): array
    {
        if (preg_match_all('/\{\{media.+?url\s*=\s*("|&quot;)(.+?)("|&quot;).*?\}\}/', $content, $matches)) {
            return $matches[2];
        }

        return [];
    }

    public function getStoreCodes($storeIds): array
    {
        $return = [];

        foreach ($storeIds as $storeId) {
            $return[] = $this->storeRepositoryInterface->getById($storeId)->getCode();
        }

        return $return;
    }

    protected function _getPageKey(PageInterface $pageInterface): string
    {
        $keys = $this->getStoreCodes($pageInterface->getStoreId());
        $keys[] = $pageInterface->getIdentifier();

        return implode(':', $keys);
    }

    public function blocksToArray(array $blockInterfaces): array //seting and returning multiple cms blocks in the form of array
    {
        $blocks = [];
        $media = [];

        foreach ($blockInterfaces as $blockInterface) {
            $blockInfo = $this->blockToArray($blockInterface);
            $blocks[$this->_getBlockKey($blockInterface)] = $blockInfo;
            $media = array_merge($media, $blockInfo['media']);
        }

        return [
            'blocks' => $blocks,
            'media' => $media,
        ];
    }

    public function blockToArray(BlockInterface $blockInterface): array //seting and returning singke cms block in the form of array
    {
        $media = $this->getMediaAttachments($blockInterface->getContent()); // for extract attachments

        $payload = [
            'cms' => [
                BlockInterface::IDENTIFIER => $blockInterface->getIdentifier(),
                BlockInterface::TITLE => $blockInterface->getTitle(),
                BlockInterface::CONTENT => $blockInterface->getContent(),
                BlockInterface::IS_ACTIVE => $blockInterface->isActive(),
            ],
            'stores' => $this->getStoreCodes($blockInterface->getStoreId()),
            'media' => $media,
        ];

        return $payload;
    }

    protected function _getBlockKey(BlockInterface $blockInterface): string
    {
        $keys = $this->getStoreCodes($blockInterface->getStoreId());
        $keys[] = $blockInterface->getIdentifier();

        return implode(':', $keys);
    }

    public function importFromZipFile($fileName, $rm = false): int
    {
        $zipArchive = new \ZipArchive(); // for unzipping the file
        $res = $zipArchive->open($fileName);
        if ($res !== true) {
            throw new \Exception('Cannot open ZIP archive');
        }

        $subPath = md5('256',date(DATE_RFC2822));
        $extractPath = $this->filesystem->getExtractPath($subPath);

        $zipArchive->extractTo($extractPath);
        $zipArchive->close();

        $pagesFile = $extractPath . '/' . self::JSON_FILENAME;
        if (!$this->file->fileExists($pagesFile, true)) {
            throw new \Exception(self::JSON_FILENAME . ' is missing');
        }

        $jsonString = $this->file->read($pagesFile);
        $cmsData = $this->decoderInterface->decode($jsonString);

        $count = $this->importFromArray($cmsData, $extractPath);

        if ($rm) {
            $this->file->rm($fileName);
        }

        $this->file->rmdir($extractPath, true);

        return $count;
    }

    public function importFromArray(array $payload, $archivePath = null): int // for importing multiple record from array
    {
        if (!isset($payload['pages']) && !isset($payload['blocks'])) {
            throw new \Exception('Invalid json archive');
        }

        $count = 0;

        foreach ($payload['pages'] as $key => $pageData) { // for importing pages
            if ($this->importPageFromArray($pageData)) {
                $count++;
            }
        }

        foreach ($payload['blocks'] as $key => $blockData) { // for importing blocks
            if ($this->importBlockFromArray($blockData)) {
                $count++;
            }
        }

        if ($archivePath && ($count > 0)) { // for importing media files
            foreach ($payload['media'] as $mediaFile) {
                $sourceFile = $archivePath . '/' . self::MEDIA_ARCHIVE_PATH . '/' . $mediaFile;
                $destFile = $this->filesystem->getMediaPath($mediaFile);

                if ($this->file->fileExists($sourceFile, true)) {
                    if (!$this->file->fileExists(dirname($destFile), false)) {
                        if (!$this->file->mkdir(dirname($destFile))) {
                            throw new \Exception('Unable to create folder: ' . dirname($destFile));
                        }
                    }
                    if (!$this->file->cp($sourceFile, $destFile)) {
                        throw new \Exception('Unable to save image: ' . $mediaFile);
                    }
                    $count++;
                }
            }
        }

        return $count;
    }

   
    public function importPageFromArray(array $pageData): bool // for importing single page
    {

        $storeIds = $this->getStoreIdsByCodes($this->mapStores($pageData['stores']));

        $collection = $this->pageCollectionFactory->create();
        $collection->addFieldToFilter(PageInterface::IDENTIFIER, $pageData['cms'][PageInterface::IDENTIFIER]);

        $pageId = 0;
        foreach ($collection as $item) {
            $storesIntersect = array_intersect($item->getStoreId(), $storeIds);

            if (count($storesIntersect)) {
                $pageId = $item->getId();
                break;
            }
        }

        $page = $this->pageFactory->create();
        if ($pageId) {
            $page->load($pageId);
        }

        $cms = $pageData['cms'];

        $page
            ->setIdentifier($cms[PageInterface::IDENTIFIER])
            ->setTitle($cms[PageInterface::TITLE])
            ->setPageLayout($cms[PageInterface::PAGE_LAYOUT])
            ->setMetaKeywords($cms[PageInterface::META_KEYWORDS])
            ->setMetaDescription($cms[PageInterface::META_DESCRIPTION])
            ->setContentHeading($cms[PageInterface::CONTENT_HEADING])
            ->setContent($cms[PageInterface::CONTENT])
            ->setSortOrder($cms[PageInterface::SORT_ORDER])
            ->setLayoutUpdateXml($cms[PageInterface::LAYOUT_UPDATE_XML])
            ->setCustomTheme($cms[PageInterface::CUSTOM_THEME])
            ->setCustomRootTemplate($cms[PageInterface::CUSTOM_ROOT_TEMPLATE])
            ->setCustomLayoutUpdateXml($cms[PageInterface::CUSTOM_LAYOUT_UPDATE_XML])
            ->setCustomThemeFrom($cms[PageInterface::CUSTOM_THEME_FROM])
            ->setCustomThemeTo($cms[PageInterface::CUSTOM_THEME_TO])
            ->setIsActive($cms[PageInterface::IS_ACTIVE]);

        $page->setData('stores', $storeIds);
        $page->save();

        return true;
    }

    public function getStoreIdsByCodes(array $storeCodes): array
    {
        $return = [];
        foreach ($storeCodes as $storeCode) {
            if ($storeCode == 'admin') {
                $return[] = 0;
            } else {
                $store = $this->storeRepositoryInterface->get($storeCode);
                if ($store && $store->getId()) {
                    $return[] = $store->getId();
                }
            }
        }

        return $return;
    }

    protected function mapStores($storeCodes): array
    {
        $return = [];
        foreach ($storeCodes as $storeCode) {
            foreach ($this->storesMap as $to => $from) {
                if ($storeCode == $from) {
                    $return[] = $to;
                }
            }
        }

        return $return;
    }


    public function importBlockFromArray(array $blockData): bool
    {

        $storeIds = $this->getStoreIdsByCodes($this->mapStores($blockData['stores']));

        $collection = $this->blockCollectionFactory->create();
        $collection->addFieldToFilter(BlockInterface::IDENTIFIER, $blockData['cms'][BlockInterface::IDENTIFIER]);

        $blockId = 0;
        foreach ($collection as $item) {
            $storesIntersect = array_intersect($item->getStoreId(), $storeIds);

            if (count($storesIntersect)) {
                $blockId = $item->getId();
                break;
            }
        }

        $block = $this->blockFactory->create();
        if ($blockId) {
            $block->load($blockId);
        }

        $cms = $blockData['cms'];

        $block->setIdentifier($cms[BlockInterface::IDENTIFIER])->setTitle($cms[BlockInterface::TITLE])->setContent($cms[BlockInterface::CONTENT])->setIsActive($cms[BlockInterface::IS_ACTIVE]);

        $block->setData('stores', $storeIds);
        $block->save();

        return true;
    }

    public function setStoresMap(array $storesMap): ContentInterface
    {
        return $this;
    }
}
