<?php

namespace Esparksinc\BulkCmsImportExport\Model;

use Magento\Framework\Filesystem\DriverInterface;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\App\Filesystem\DirectoryList;

class Filesystem
{
    const EXPORT_PATH = 'Esparksinc_BulkCmsImportExport/export';
    const EXTRACT_PATH = 'Esparksinc_BulkCmsImportExport/extract';
    const UPLOAD_PATH = 'Esparksinc_BulkCmsImportExport/extract';

    protected $filesystem;
    protected $file;

    public function __construct(
        \Magento\Framework\Filesystem $filesystem,
        File $file
    ) {
        $this->filesystem = $filesystem;
        $this->file = $file;
    }

    public function getUploadPath()
    {
        $varDir = $this->filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $exportPath = $varDir->getAbsolutePath(self::UPLOAD_PATH);

        $this->file->mkdir($exportPath, DriverInterface::WRITEABLE_DIRECTORY_MODE, true);
        return $exportPath;
    }

    public function getExportPath()
    {
        $varDir = $this->filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $exportPath = $varDir->getAbsolutePath(self::EXPORT_PATH);

        $this->file->mkdir($exportPath, DriverInterface::WRITEABLE_DIRECTORY_MODE, true);
        return $exportPath;
    }

    public function getExtractPath($subPath)
    {
        $varDir = $this->filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $extractPath = $varDir->getAbsolutePath(self::EXTRACT_PATH.'/'.$subPath);

        $this->file->mkdir($extractPath, DriverInterface::WRITEABLE_DIRECTORY_MODE, true);
        return $extractPath;
    }

    public function getMediaPath($mediaFile, $write = false)
    {
        if ($write) {
            $mediaDir = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        } else {
            $mediaDir = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
        }

        return $mediaDir->getAbsolutePath($mediaFile);
    }
}
